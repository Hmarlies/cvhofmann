/**
 * plugins/vuetify.js
 *
 * Framework documentation: https://vuetifyjs.com`
 */

// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Composables
import { createVuetify } from 'vuetify'

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
  theme: {
    themes: {
      light: {
        colors: {
          time: '#7A89C2',
          graduation: '#D0D9FF',
          work: '#D5D96C',
          navBar: '#7A89C2',
          navBarText: '#000000',
          basic: '#D0D9FF'
        },
      },
    },
  },
},
)
