// Utilities
import {defineStore} from 'pinia'

export const useJsonStore = defineStore('json', () => {

  const graduations = [
    {year: 2010, school: "HBLA", exam: "Matura"},
    {year: 2021, school: "WU", exam: "Bachelor"},
    {year: 2023, school: "Hallo", exam: "Hallo"},
    {year: 2021, school: "", exam: "LAP"}
  ];

  const summerJobs = [
    {year: 2005, month: "Juli", company: "Filino", job: "Näherin"},
    {year: 2008, month: "Juli", company: "Taborland", job: "Fleischverpackung"}
  ]

  const workStudent = [
    {beginnYear: 2005, work: "Regalbetreuung", endYear: 2008},
    {beginnYear: 2008, work: "Zimmermädchen", endYear: 2010},
    {beginnYear: 2010, work: "Bauhaus", endYear: 2014}
  ]

  const toLinkList = [
    {link: '/', text: 'CV'},
    {link: '/certification', text: 'Zeugnisse'},
    {link: '/aboutMe', text: 'Über Mich'},
    {link: '/git', text: 'Git Repository'},
    {link: '/cvPDF', text: 'CV PDF'}
  ]


  return {workStudent, graduations, summerJobs, toLinkList};
})
