// Utilities
import { defineStore } from 'pinia'


export const useFunctionStore = defineStore('function', () => {

    function range(start, end) {
        let ans = [];
        for (let i = end; i >= start; i--) {
            ans.push(i);
        }
        return ans;
    }

    return { range  };

})
