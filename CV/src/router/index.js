// Composables
import {createRouter, createWebHistory} from 'vue-router';
import HomeView from '@/views/HomeView.vue';
import AboutMeView from '@/views/AboutMeView.vue';
import CertificationView from '@/views/CertificationView.vue';
import GitRepositoryView from '@/views/GitRepositoryView.vue';
import CvPDFView from "@/views/CvPDFView.vue";

const routes = [
  {
    path: '/',
    component: HomeView
  },
  {
    path: '/aboutMe',
    component: AboutMeView
  },
  {
    path: '/certification',
    component: CertificationView
  },
  {
    path: '/git',
    component: GitRepositoryView
  },
  {path: '/cvPDF',
  component: CvPDFView}

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
